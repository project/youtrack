<?php

namespace Drupal\youtrack_ui\Form;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\youtrack\API\ConnectionManager;
use YouTrack\Exception;

/**
 * Class YouTrackSettingsForm
 *
 * @package Drupal\youtrack_ui\Form
 */
class YouTrackSettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\youtrack\API\ConnectionManager
   */
  protected $youtrackConnection;

  /**
   * YouTrackSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\youtrack\API\ConnectionManager $youtrack_connection
   */
  public function __construct(ConfigFactoryInterface $config_factory, ConnectionManager $youtrack_connection) {
    parent::__construct($config_factory);
    $this->youtrackConnection = $youtrack_connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('youtrack.connection')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['youtrack.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'youtrack_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('youtrack.settings');

    $form['youtrack_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#description' => $this->t('YouTrack instance URL'),
      '#default_value' => $config->get('youtrack_url'),
    );

    $form['youtrack_login'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('API User Login'),
      '#default_value' => $config->get('youtrack_login'),
    );

    $form['youtrack_password'] = array(
      '#type' => 'password',
      '#title' => $this->t('API User Password'),
      '#default_value' => $config->get('youtrack_password'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      $password = $form_state->getValue('youtrack_password') ?? $this->config('youtrack.settings')->get('youtrack_password');

      $this->youtrackConnection->connect(
        $form_state->getValue('youtrack_url'),
        $form_state->getValue('youtrack_login'),
        $password
      );
    }
    catch (Exception $e) {
      $form_state->setError($form, $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config('youtrack.settings');
    $config
      ->set('youtrack_url', $form_state->getValue('youtrack_url'))
      ->set('youtrack_login', $form_state->getValue('youtrack_login'));
    if ($form_state->getValue('youtrack_password')) {
      $config->set('youtrack_password', $form_state->getValue('youtrack_password'));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }
}
