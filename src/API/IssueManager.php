<?php

namespace Drupal\youtrack\API;

use YouTrack\Issue;

class IssueManager {

  /**
   * @var \Drupal\youtrack\API\ConnectionManager
   */
  protected $connectionManager;

  /**
   * Constructs a IssuesManager object.
   *
   * @param ConnectionManager $connection_manager
   */
  public function __construct(ConnectionManager $connection_manager) {
    $this->connectionManager = $connection_manager;
  }

  /**
   * Get list of accessible projects.
   *
   * @param $project
   * @param $summary
   * @param $description
   * @param $commands
   *
   * @return \YouTrack\Issue
   * @throws \YouTrack\Exception
   */
  public function createIssue($project, $summary, $description, $commands): Issue {
    $connection = $this->connectionManager->getConnection();

    $issue = $connection->createIssue($project, $summary, array('description' => $description));

    if (!empty($commands)) {
      $connection->executeCommand($issue->id, $commands);
    }

    return $issue;
  }
}
