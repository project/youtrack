<?php

namespace Drupal\youtrack\API;

class ProjectManager {

  /**
   * @var \Drupal\youtrack\API\ConnectionManager
   */
  protected $connectionManager;

  /**
   * Constructs a ProjectsManager object.
   *
   * @param ConnectionManager $connection_manager
   */
  public function __construct(ConnectionManager $connection_manager) {
    $this->connectionManager = $connection_manager;
  }

  /**
   * Get list of accessible projects.
   */
  public function getAccessibleProjects(): array {
    $projects = $this->connectionManager->getConnection()->getAccessibleProjects();

    $accessible_projects = array();
    foreach ($projects as $project) {
      $accessible_projects[$project->getShortName()] = $project->getName();
    }

    return $accessible_projects;
  }
}
